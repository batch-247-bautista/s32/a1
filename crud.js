



// Use the "require" directive to load Node.js modules
let http = require("http");

http.createServer(function (request, response) {

      
      // The HTTP Method of the incoming request can be accesed via the "method" property of the "request" parameter
      // The Method "GET" means that we will be retrieving or reading information
      if(request.url == "/homepage" && request.method == "GET"){

            // Request the "/items" path and "GETS" information
            response.writeHead(200, {'Content-Type': 'text/plain'});
            // Ends the reponse process
            response.end('Welcome to booking system!');
      }

      if(request.url == "/profile" && request.method == "GET"){

            // Request the "/items" path and "GETS" information
            response.writeHead(200, {'Content-Type': 'text/plain'});
            // Ends the reponse process
            response.end('Welcome to your profile!');
      }

      if(request.url == "/Courses" && request.method == "GET"){

            // Request the "/items" path and "GETS" information
            response.writeHead(200, {'Content-Type': 'text/plain'});
            // Ends the reponse process
            response.end("Here's our courses available!");
      }

      // The method "POST" means that we will be adding or creating information
      // In this example, we will just be sending a text response for now
      if(request.url == "/addCourse" && request.method == "POST"){

            // Request the "/items" path and "SENDS" information
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.end('Add course to our resources!');
      }
      if(request.url == "/updateCourse" && request.method == "PUT"){

            // Request the "/items" path and "SENDS" information
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.end('Update course to our resources!');
      }

      // The method "POST" means that we will be adding or creating information
      // In this example, we will just be sending a text response for now
      if(request.url == "/archiveCourse" && request.method == "DELETE"){

            // Request the "/items" path and "SENDS" information
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.end('Archive course to our resources!');
      }
}).listen(4005);

console.log('Server running at localhost:4005');
